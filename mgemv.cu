#include <unistd.h>
#include <iostream>
#include <stdlib.h>
#include <assert.h>
#include <cuda_runtime.h>
#include <cublas_v2.h>
#include <sched.h>
#include <vector>

using namespace std;

// #define FP16MM

// Convenience function for checking CUDA runtime API results
// can be wrapped around any runtime API call. No-op in release builds.
inline
cudaError_t checkCuda(cudaError_t result)
{
  if (result != cudaSuccess) {
    fprintf(stderr, "CUDA Runtime Error: %s\n", cudaGetErrorString(result));
    assert(result == cudaSuccess);
  }
  return result;
}
const char* cublasGetErrorString(cublasStatus_t status)
{
    switch(status)
    {
        case CUBLAS_STATUS_SUCCESS: return "CUBLAS_STATUS_SUCCESS";
        case CUBLAS_STATUS_NOT_INITIALIZED: return "CUBLAS_STATUS_NOT_INITIALIZED";
        case CUBLAS_STATUS_ALLOC_FAILED: return "CUBLAS_STATUS_ALLOC_FAILED";
        case CUBLAS_STATUS_INVALID_VALUE: return "CUBLAS_STATUS_INVALID_VALUE"; 
        case CUBLAS_STATUS_ARCH_MISMATCH: return "CUBLAS_STATUS_ARCH_MISMATCH"; 
        case CUBLAS_STATUS_MAPPING_ERROR: return "CUBLAS_STATUS_MAPPING_ERROR";
        case CUBLAS_STATUS_EXECUTION_FAILED: return "CUBLAS_STATUS_EXECUTION_FAILED"; 
        case CUBLAS_STATUS_INTERNAL_ERROR: return "CUBLAS_STATUS_INTERNAL_ERROR"; 
    }
    return "unknown error";
}

inline
cublasStatus_t checkCublas(cublasStatus_t result)
{
  if (result != CUBLAS_STATUS_SUCCESS) {
    fprintf(stderr, "CUDA Runtime Error: %s\n", cublasGetErrorString(result));
    assert(result == CUBLAS_STATUS_SUCCESS);
  }
  return result;
}
struct GpuObj {
  std::vector<int> data_dims;
  float *d_data;
  int nb_elem;

  GpuObj(int m, int n) {
    data_dims = std::vector<int>(2);
    data_dims[0] = m;
    data_dims[1] = n;
    nb_elem = data_dims[0] * data_dims[1];
    checkCuda(cudaMalloc(&d_data, nb_elem * sizeof(float)));
  }

  ~GpuObj() {
    cudaFree(d_data);
  }
};


// Fill the array A(nr_rows_A, nr_cols_A) with random numbers on CPU
void CPU_fill_rand(float *A, int nr_rows_A, int nr_cols_A) {
	int a=1;

    for(int i = 0; i < nr_rows_A * nr_cols_A; i++){
		A[i] = (float)rand()/(float)(RAND_MAX/a);
	}
}

int main(int argc, char ** argv){
  std::cout << "Set scheduler..." << std::endl;
  struct sched_param sp = { .sched_priority = 49 };
  int ret;

  ret = sched_setscheduler(0, SCHED_FIFO, &sp);
  if (ret == -1) {
    perror("sched_setscheduler");
    return 1;
  }
  std::cout << "Scheduler set" << std::endl;

  int m = 4312; // Typical size for our application
  int n = 24416;
  int ngpus = 4;
  int subsize = n / ngpus; // for simplicity, n must be a multiple of ngpus 

  int repeats = 10000;
  int verbose = 1;
  
  if(verbose) 
    cout << "running with" 
	 << " m: " << m
	 << " n: " << n
	 << " repeats: " << repeats
	 << endl;

  cublasStatus_t stat;
  cublasHandle_t handle;
  //cudaSetDeviceFlags(cudaDeviceScheduleSpin);

  // Enabling peer-to-peer between devices
  checkCuda(cudaSetDevice(0));
  checkCuda(cudaDeviceEnablePeerAccess(1, 0));
  checkCuda(cudaDeviceEnablePeerAccess(2, 0));
  checkCuda(cudaDeviceEnablePeerAccess(3, 0));
  checkCuda(cudaSetDevice(1));
  checkCuda(cudaDeviceEnablePeerAccess(0, 0));
  checkCuda(cudaDeviceEnablePeerAccess(2, 0));
  checkCuda(cudaDeviceEnablePeerAccess(3, 0));
  checkCuda(cudaSetDevice(2));
  checkCuda(cudaDeviceEnablePeerAccess(0, 0));
  checkCuda(cudaDeviceEnablePeerAccess(1, 0));
  checkCuda(cudaDeviceEnablePeerAccess(3, 0));
  checkCuda(cudaSetDevice(3));
  checkCuda(cudaDeviceEnablePeerAccess(0, 0));
  checkCuda(cudaDeviceEnablePeerAccess(2, 0));
  checkCuda(cudaDeviceEnablePeerAccess(1, 0));

  
  cudaEvent_t event[ngpus];
  for(int i=0; i<ngpus; i++) {
    cudaSetDevice(i);
    cudaEventCreate(&event[i]);
  }
  checkCublas(cublasCreate(&handle));
  if(verbose) cout << "allocating device variables" << endl;
  cudaSetDevice(0);
  
  // Allocate 3 arrays on CPU
  
  float *h_A = (float *)malloc(m * n * sizeof(float));
  float *h_B = (float *)malloc(n * sizeof(float));
  float *h_C = (float *)malloc(m * sizeof(float));
  
  CPU_fill_rand(h_A, m, n);
  CPU_fill_rand(h_B, n, 1);
  CPU_fill_rand(h_C, m, 1);

  // Allocate arrays on GPU 0
  cout << "allocating device 0 variables" << endl;
  GpuObj d_A(m, n), d_B(n,1), d_C(m,1);
  cout << "memcpy on device 0" << endl;

  checkCuda(cudaMemcpy(d_A.d_data,h_A,d_A.nb_elem * sizeof(float),cudaMemcpyHostToDevice));
  checkCuda(cudaMemcpy(d_B.d_data,h_B, d_B.nb_elem * sizeof(float),cudaMemcpyHostToDevice));
  checkCuda(cudaMemcpy(d_C.d_data,h_C, d_C.nb_elem * sizeof(float),cudaMemcpyHostToDevice));
  
  // Allocate arrays on others GPU
  int cpt = subsize * m;
  std::vector<GpuObj *> d_submats; // Sub part of the matrix
  std::vector<GpuObj *> d_subres; // Sub result of the MVM
  d_submats.push_back(&d_A);
  d_subres.push_back(&d_C);

  for (int k=1; k < ngpus ; k++) {
    checkCuda(cudaSetDevice(k));
    d_subres.push_back(new GpuObj(m, 1));
    d_submats.push_back(new GpuObj(m, subsize));
    checkCuda(cudaMemcpy(d_submats[k]->d_data, d_A.d_data + cpt, d_submats[k]->nb_elem * sizeof(float),cudaMemcpyDeviceToDevice));
    cpt += subsize * m;
  }
    int lda, ldb, ldc;
    const float alf = 1.0f;
    const float bet = 0.0f;
    const float *alpha = &alf;
    const float *beta = &bet;
    lda = m;
    ldb = n;
    ldc = m;
  
  cudaEvent_t start, stop;
  cudaSetDevice(0);
  cudaEventCreate(&start);
  cudaEventCreate(&stop);
  // WARM-UP

  std::cout << "Warm-up" << std::endl;
  for (int k = 0 ; k < ngpus ; k++) {
    cudaSetDevice(k);
    stat = cublasSgemv(handle, CUBLAS_OP_N, m, subsize, &alf, d_submats[k]->d_data, m, d_B.d_data, 1, &bet, d_subres[k]->d_data, 1);
    // cudaStreamSynchronize(0);
    if(stat != CUBLAS_STATUS_SUCCESS){
      cerr << "warmup failed on device " << k << endl;
      exit(1);
          }
  }

    double sum = 0.0;
    std::cout << "Begin reps" << std::endl;
    for(int rep = 0; rep < repeats; rep++){
      cudaSetDevice(0);
      cudaEventRecord(start, 0);
      // Start MVM on devices > 0
      for (int k = 1 ; k < ngpus ; k++) {
        cudaSetDevice(k);
        stat = cublasSgemv(handle, CUBLAS_OP_N, m, subsize, &alf, d_submats[k]->d_data, m, d_B.d_data, 1, &bet, d_subres[k]->d_data, 1);
        cudaEventRecord(event[k], 0);
      }
      // MVM on device 0
      cudaSetDevice(0);
      stat = cublasSgemv(handle, CUBLAS_OP_N, m, subsize, &alf, d_submats[0]->d_data, m, d_B.d_data, 1, &bet, d_subres[0]->d_data, 1);

      for(int k=1; k < ngpus; k++) {
        cudaStreamWaitEvent(0, event[k], 0);
        stat = cublasSaxpy(handle, m, &alf, d_subres[k]->d_data, 1, d_subres[0]->d_data, 1); // Gather results on device 0
      }

      // cudaStreamSynchronize(0);

      cudaEventRecord(stop,0);
      cudaEventSynchronize(stop);
      if(stat != CUBLAS_STATUS_SUCCESS){
	cerr << "cublasSgemv failed" << endl;
	exit(1);
      }
      assert(!cudaGetLastError());
      
      float elapsed;
      cudaEventElapsedTime(&elapsed, start, stop);
      elapsed *= 1000.0f;
      sum += elapsed;

    }
    cout <<  "average: " << sum/repeats << " µs "<< endl;


  // Free CPU memory
  free(h_A);
  free(h_B);
  free(h_C);
      
  return 0;
}
